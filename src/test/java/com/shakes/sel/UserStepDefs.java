package com.shakes.sel;

import static org.junit.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
//may05
public class UserStepDefs {
	WebDriver driver = Hooks.driver;
	
	@Given("^the user is on the home page$")
	public void the_user_is_on_the_home_page() {
		driver.get("http://localhost:8080/shakes/");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@When("^the user enters the url$")
	public void the_user_enters_the_url() {
		driver.get("http://localhost:8080/shakes/");
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Then("^the use should see \"([^\"]*)\"$")
	public void the_use_should_see(String arg1) {
	   WebElement warning = driver.findElement(By.id("mainBody"));
	   assertEquals(warning.getText(), "Welcome to Shakes consultancy");
	}
}
