package com.shakes.arq;

import static org.junit.Assert.assertEquals;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.shakes.shakes.model.User;

@RunWith(Arquillian.class)
public class arqTest {
	@Deployment
	public static Archive<?> CreateTestArchieve() {
		return ShrinkWrap.create(JavaArchive.class, "Test.jar").addClasses(User.class)
				.addAsManifestResource("META-INF/persistence.xml", "persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Test
	public void exampleTest() {
		assertEquals(true, true);
	}
}
