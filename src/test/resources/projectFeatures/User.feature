Feature: User access check
  As an admin or user
  I want to access the home page
  So I can use the web app
  
Background:
	Given the user is on the home page
	
Scenario: the user should be able to login
	When the user enters the url
	Then the use should see "Welcome to Shakes consultanty"
